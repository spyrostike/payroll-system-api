module.exports = {
  semi: false,
  trailingComma: 'all',
  singleQuote: true,
  printWidth: 70,
  tabWidth: 4,
  singleQuote: true,
}
