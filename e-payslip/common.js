(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "XkzH":
/*!***********************************************!*\
  !*** ./src/app/core/services/mail.service.ts ***!
  \***********************************************/
/*! exports provided: MailService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MailService", function() { return MailService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class MailService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getListFile() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip`, { headers: httpHeaders }).toPromise();
        });
    }
    getMailTransByFileId(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/mail/mail-payslip-transaction/${id}`, { headers: httpHeaders }).toPromise();
        });
    }
    getByPage(dateFrom, dateTo, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/mail/mail-payslip-transaction?dateFrom=${dateFrom}&dateTo=${dateTo}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getMailTransByRangeDate(dateFrom, dateTo) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/mail/mail-payslip-transaction?dateFrom=${dateFrom}&dateTo=${dateTo}`, { headers: httpHeaders }).toPromise();
        });
    }
    sendmail(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/mail/send-pay-slip/${id}`, data, { headers: httpHeaders });
    }
    sendmailUser(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/mail/send-pay-slip-user/${id}`, data, { headers: httpHeaders });
    }
    generatePeriodMouths() {
        const mouths = [
            { value: '01', viewValue: 'มกราคม' },
            { value: '02', viewValue: 'กุมภาพันธ์' },
            { value: '03', viewValue: 'มีนาคม' },
            { value: '04', viewValue: 'เมษายน' },
            { value: '05', viewValue: 'พฤษภาคม' },
            { value: '06', viewValue: 'มิถุนายน' },
            { value: '07', viewValue: 'กรกฎาคม' },
            { value: '08', viewValue: 'สิงหาคม' },
            { value: '09', viewValue: 'กันยายน' },
            { value: '10', viewValue: 'ตุลาคม' },
            { value: '11', viewValue: 'พฤศจิกายน' },
            { value: '12', viewValue: 'ธันวาคม' }
        ];
        return mouths;
    }
    generatePeriodYears() {
        const current = new Date();
        const curYear = current.getFullYear();
        const years = [];
        for (let i = -5; i <= 0; i++) {
            let year = curYear + i;
            let date = new Date(year, 1);
            let yearEN = date.getFullYear();
            let yearTH = new Intl.DateTimeFormat('th-TH', { year: 'numeric' }).format(date);
            let map = { value: yearTH.substr(yearTH.length - 4, yearTH.length), viewValue: yearTH };
            years.push(map);
        }
        return years;
    }
}
MailService.ɵfac = function MailService_Factory(t) { return new (t || MailService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
MailService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: MailService, factory: MailService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](MailService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "ifH6":
/*!************************************************!*\
  !*** ./src/app/core/services/admin.service.ts ***!
  \************************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");





class AdminService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getAll(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/users`, { headers: httpHeaders });
    }
    getByPage(textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin?textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getById(id) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin/${id}`, { headers: httpHeaders });
    }
    getByText(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin?textSearch=${textSearch}&status=${status}&perPage=${perPage}`, { headers: httpHeaders });
    }
    registerAdmin(user) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin`, user, { headers: httpHeaders });
    }
    editUserById(id, user) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin/${id}`, user, { headers: httpHeaders });
    }
    deleteUserById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin/${id}`, { headers: httpHeaders });
    }
    changePassword(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl}/admin/${id}`, data, { headers: httpHeaders });
    }
}
AdminService.ɵfac = function AdminService_Factory(t) { return new (t || AdminService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
AdminService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AdminService, factory: AdminService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdminService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "sINK":
/*!***********************************************!*\
  !*** ./src/app/core/services/file.service.ts ***!
  \***********************************************/
/*! exports provided: FileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileService", function() { return FileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class FileService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getListFile() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip`, { headers: httpHeaders }).toPromise();
        });
    }
    getByPage(dateFrom, dateTo, textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip?dateFrom=${dateFrom}&dateTo=${dateTo}&textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getListFileByRangeDate(dateFrom, dateTo) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip?dateFrom=${dateFrom}&dateTo=${dateTo}`, { headers: httpHeaders }).toPromise();
        });
    }
    getFileById(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip/${id}`, { headers: httpHeaders }).toPromise();
        });
    }
    uploadFile(formData) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/upload-pay-slip`, formData, { headers: httpHeaders }).toPromise();
        });
    }
    getTransSendMailByPage(dateFrom, dateTo, textSearch, status, currentPage, perPage) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip-user?dateFrom=${dateFrom}&dateTo=${dateTo}&textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders }).toPromise();
        });
    }
    getTransSendMailByPage1(dateFrom, dateTo, textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip-user?dateFrom=${dateFrom}&dateTo=${dateTo}&textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    deleteFileById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/file/pay-slip/${id}`, { headers: httpHeaders });
    }
}
FileService.ɵfac = function FileService_Factory(t) { return new (t || FileService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
FileService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: FileService, factory: FileService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](FileService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=common.js.map