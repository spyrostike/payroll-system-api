(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-dashboard-dashboard-module~pages-manage-user-manage-user-module"],{

/***/ "2NMq":
/*!*************************************************!*\
  !*** ./src/app/core/services/branch.service.ts ***!
  \*************************************************/
/*! exports provided: BranchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BranchService", function() { return BranchService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class BranchService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getAll() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch?textSearch=&status=active&perPage=100000`, { headers: httpHeaders }).toPromise();
        });
    }
    getByPage(textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch?textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getByText(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch?textSearch=${textSearch}&status=${status}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getById(id) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch/${id}`, { headers: httpHeaders });
    }
    add(data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch`, data, { headers: httpHeaders });
    }
    edit(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch/${id}`, data, { headers: httpHeaders });
    }
    deleteById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/branch/${id}`, { headers: httpHeaders });
    }
}
BranchService.ɵfac = function BranchService_Factory(t) { return new (t || BranchService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
BranchService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: BranchService, factory: BranchService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](BranchService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "T6lX":
/*!***************************************************!*\
  !*** ./src/app/core/services/position.service.ts ***!
  \***************************************************/
/*! exports provided: PositionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionService", function() { return PositionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class PositionService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getAll() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position?textSearch=&status=active&perPage=100000`, { headers: httpHeaders }).toPromise();
        });
    }
    getByPage(textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position?textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getById(id) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position/${id}`, { headers: httpHeaders });
    }
    getByText(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position?textSearch=${textSearch}&status=${status}&perPage=${perPage}`, { headers: httpHeaders });
    }
    add(data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position`, data, { headers: httpHeaders });
    }
    edit(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position/${id}`, data, { headers: httpHeaders });
    }
    deleteById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/position/${id}`, { headers: httpHeaders });
    }
}
PositionService.ɵfac = function PositionService_Factory(t) { return new (t || PositionService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
PositionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: PositionService, factory: PositionService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](PositionService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "XWna":
/*!*****************************************************!*\
  !*** ./src/app/core/services/department.service.ts ***!
  \*****************************************************/
/*! exports provided: DepartmentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepartmentService", function() { return DepartmentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class DepartmentService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getAll() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department?textSearch=&status=active&perPage=100000`, { headers: httpHeaders }).toPromise();
        });
    }
    getByPage(textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department?textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getById(id) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department/${id}`, { headers: httpHeaders });
    }
    getByText(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department?textSearch=${textSearch}&status=${status}&perPage=${perPage}`, { headers: httpHeaders });
    }
    add(data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department`, data, { headers: httpHeaders });
    }
    edit(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department/${id}`, data, { headers: httpHeaders });
    }
    deleteById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/department/${id}`, { headers: httpHeaders });
    }
}
DepartmentService.ɵfac = function DepartmentService_Factory(t) { return new (t || DepartmentService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
DepartmentService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: DepartmentService, factory: DepartmentService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](DepartmentService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "f4AX":
/*!***********************************************!*\
  !*** ./src/app/core/services/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");






class UserService {
    constructor(http) {
        this.http = http;
    }
    getHearder() {
        const accessToken = JSON.parse(sessionStorage.getItem('accessToken'));
        const internalToken = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].internalToken}`;
        const appId = `${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].appId}`;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            "internal-token": internalToken,
            appId: appId,
            accessToken: accessToken
        });
        return httpHeaders;
    }
    getAll(textSearch, status, perPage) {
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/users`);
    }
    getByPage(textSearch, status, currentPage, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user?textSearch=${textSearch}&status=${status}&currentPage=${currentPage}&perPage=${perPage}`, { headers: httpHeaders });
    }
    getById(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/${id}`, { headers: httpHeaders }).toPromise();
        });
    }
    getByText(textSearch, status, perPage) {
        const httpHeaders = this.getHearder();
        return this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user?textSearch=${textSearch}&status=${status}&perPage=${perPage}`, { headers: httpHeaders });
    }
    registerUser(data) {
        const httpHeaders = this.getHearder();
        return this.http.post(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user`, data, { headers: httpHeaders });
    }
    editUserById(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/${id}`, data, { headers: httpHeaders });
    }
    deleteUserById(id) {
        const httpHeaders = this.getHearder();
        return this.http.delete(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/${id}`, { headers: httpHeaders });
    }
    getPaySlipUser(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const httpHeaders = this.getHearder();
            return yield this.http.get(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/pay-slip-user`, { headers: httpHeaders }).toPromise();
        });
    }
    changePassword(id, data) {
        const httpHeaders = this.getHearder();
        return this.http.put(`${_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl}/user/${id}`, data, { headers: httpHeaders });
    }
}
UserService.ɵfac = function UserService_Factory(t) { return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: UserService, factory: UserService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](UserService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=default~pages-dashboard-dashboard-module~pages-manage-user-manage-user-module.js.map