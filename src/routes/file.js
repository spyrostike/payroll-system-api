import { Router } from 'express'
import multer from 'multer'
import dayjs from 'dayjs'
import path from 'path'
import { checkAndMkDir } from '../utils/dirHelper'
import { getPaySlip, getPaySlipRecord, getPaySlipByUserId, uploadPaySlip, deletedPaySlip, uploadUser } from '../controllers/file-controller'
import intenalServiceVeify from '../middleware/internal'
import { jwtVerify } from '../middleware/jwt-helper'

const router = Router()

const upload = multer({
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDDHH'))
            checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = path.extname(file.originalname)
            let originName = file.originalname.split('.')
            let fileName = `${originName[0]}-${dayjs().format('YYYYMMDDHHmmss')}${mimetype}`
            callback(null, fileName)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!path.extname(file.originalname).match(/\.(csv)$/)) {
          return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT
    }
})

router.get('/pay-slip', [jwtVerify(), intenalServiceVeify()], getPaySlip)

router.get('/pay-slip/:id', [jwtVerify(), intenalServiceVeify()], getPaySlipRecord)

router.post('/upload-pay-slip', [jwtVerify(), intenalServiceVeify(), upload.single('file-upload')], uploadPaySlip)

router.delete('/pay-slip/:id', [jwtVerify(), intenalServiceVeify()], deletedPaySlip)

router.post('/upload-user', [jwtVerify(), intenalServiceVeify(), upload.single('file-upload')], uploadUser)

router.get('/pay-slip-user', [jwtVerify(), intenalServiceVeify()], getPaySlipByUserId)


export default router