import auth from './auth'
import admin from './admin'
import user from './user'
import file from './file'
import branch from './branch'
import department from './department'
import position from './position'
import mail from './mail'

const init = (app) => {
    const api_v1 = '/api/v1'

    app.use(`${api_v1}/auth`, auth)

    app.use(`${api_v1}/admin`, admin)

    app.use(`${api_v1}/user`, user)

    app.use(`${api_v1}/file`, file)

    app.use(`${api_v1}/branch`, branch)

    app.use(`${api_v1}/department`, department)

    app.use(`${api_v1}/position`, position)

    app.use(`${api_v1}/mail`, mail)

}

export default init
