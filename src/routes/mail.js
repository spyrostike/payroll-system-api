import { Router } from 'express'
import { check } from 'express-validator'
import { sendPaySlip, sendPaySlipByUser, getMailTransaction, getMailTransactionInfo } from '../controllers/mail-controller'
import intenalServiceVeify from '../middleware/internal'
import { jwtVerify } from '../middleware/jwt-helper'

const router = Router()

router.post('/send-pay-slip/:fileId', [jwtVerify(), intenalServiceVeify()], sendPaySlip)
router.post('/send-pay-slip-user/:fileId', [jwtVerify(), intenalServiceVeify()], sendPaySlipByUser)

router.get('/mail-payslip-transaction', [jwtVerify(), intenalServiceVeify()], getMailTransaction)
router.get('/mail-payslip-transaction/:id', [jwtVerify(), intenalServiceVeify()], getMailTransactionInfo)

export default router