import { Router } from 'express'
import { check } from 'express-validator'
import { admin, user } from '../controllers/auth-controller'
import intenalServiceVeify from '../middleware/internal'
import { jwtVerify } from '../middleware/jwt-helper'

const router = Router()

router.post('/admin', [intenalServiceVeify(), check('username').exists(), check('password').exists()], admin)

router.post('/user', [intenalServiceVeify(), check('username').exists(), check('password').exists()], user)

export default router