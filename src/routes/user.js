import { Router } from 'express'
import { check } from 'express-validator'
import { add, deleted, getList, getInfo, update } from '../controllers/user-controller'
import intenalServiceVeify from '../middleware/internal'
import { jwtVerify } from '../middleware/jwt-helper'

const router = Router()

router.get('/', [jwtVerify(), intenalServiceVeify()], getList)

router.get('/:id', [jwtVerify(), intenalServiceVeify()], getInfo)

router.post(
  '/',
  [
    jwtVerify(), 
    intenalServiceVeify(),
    check('username').exists(),
    check('password').exists()
  ],
  add
)

router.put('/:id', [jwtVerify(), intenalServiceVeify()], update)

router.delete('/:id', [jwtVerify(), intenalServiceVeify()], deleted)

export default router