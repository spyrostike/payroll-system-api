
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('users',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary()
            table.string('code', 15).notNullable()//.unique()
            table.string('username', 50).notNullable()
            table.string('password', 150).notNullable()
            // table.string('private_key', 100).notNullable()
            table.string('first_name', 50).notNullable()
            table.string('last_name', 50).notNullable()
            table.string('id_card_no', 13).notNullable()
            table.integer('age', 2).notNullable()
            table.text('address')
            table.string('email')
            table.string('tel')
            table.uuid('branch_id')
            table.uuid('department_id')
            table.uuid('position_id')
            table.enu('role', [ 'user' ], {
                useNative: true,
                enumName: 'USER_ROLE',
                schemaName: 'public',
            })
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.enu('gender', null, {
                useNative: true,
                enumName: 'GENDER',
                schemaName: 'public',
                existingType: true
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('updated_by')

            table.foreign('branch_id').references('id').inTable('branches')
            table.foreign('department_id').references('id').inTable('departments')
            table.foreign('position_id').references('id').inTable('positions')

        }),

    ])

}

exports.down = function(knex) {

}
