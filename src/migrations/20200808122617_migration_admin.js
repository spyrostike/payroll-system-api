
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('admins',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();
            table.string('username', 50).unique().notNullable()
            table.string('password', 150).notNullable()
            // table.string('private_key', 100).notNullable()
            table.string('first_name', 50).notNullable()
            table.string('last_name', 50).notNullable()
            table.string('email');
            table.string('tel');
            table.enu('role', [ 'super-admin', 'admin' ], {
                useNative: true,
                enumName: 'ADMIN_ROLE',
                schemaName: 'public',
            })
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.enu('gender', [ 'male', 'female' ], {
                useNative: true,
                enumName: 'GENDER',
                schemaName: 'public',
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
            table.uuid('updated_by')

        }),

    ])

};

exports.down = function(knex) {

};
