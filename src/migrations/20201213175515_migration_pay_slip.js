
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('pay_slips',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary()
            table.string('branch_code', 10)
            table.string('department_code', 10)
            table.string('department_name', 100)
            table.string('employee_code', 10)
            table.string('employee_name', 100) //5
            table.string('include_department', 100)
            table.string('include_branch', 100)
            table.float('work_day')
            table.float('salary', 14, 2)
            table.float('position_cost', 14, 2) //10
            table.float('otx1', 14, 2)
            table.float('otx1_5', 14, 2)
            table.float('otx2', 14, 2)
            table.float('otx3', 14, 2)
            table.float('ot_other', 14, 2) //15
            table.float('shift_cost', 14, 2)
            table.float('duty_cost', 14, 2)
            table.float('welfare', 14, 2)
            table.float('reward', 14, 2)
            table.float('bonus', 14, 2) //20
            table.float('other_income', 14, 2)
            table.float('salary_include_other', 14, 2)
            table.float('absent_days', 14, 2)
            table.float('late_days', 14, 2)
            table.float('deduct_work_off_early', 14, 2) //25
            table.float('deduct_worf_impaired', 14, 2)
            table.float('deduct_advance', 14, 2)
            table.float('deduct_welfare', 14, 2)
            table.float('deduct_other', 14, 2)
            table.float('deduct_tax', 14, 2) //30
            table.float('deduct_fund', 14, 2)
            table.float('deduct_social_society', 14, 2)
            table.float('security_deposit', 14, 2)
            table.float('deduct_loan', 14, 2)
            table.float('deduct_installment', 14, 2) //35
            table.float('total_deduct', 14, 2)
            table.float('salary_net', 14, 2)
            table.uuid('file_id')
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('updated_by')

            table.foreign('file_id').references('id').inTable('files')
        }),

    ])

}

exports.down = function(knex) {

}
