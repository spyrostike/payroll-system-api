
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('branches',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();
            table.string('code', 10).notNullable()
            table.string('name', 150).notNullable()
            table.enu('status', [ 'active', 'inactive' ], {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
            table.uuid('updated_by')
        }),

    ])

};

exports.down = function(knex) {

}
