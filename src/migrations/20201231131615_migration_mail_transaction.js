
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('mail_transactions',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary()
            table.uuid('file_id')
            table.integer('sent_success')
            table.integer('sent_fail')
            table.integer('sent_total')
            table.string('period', 7).notNullable()
            table.enu('category', null, {
                useNative: true,
                enumName: 'FILE_CATEGORY',
                schemaName: 'public',
                existingType: true
            })
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('updated_by')

            table.foreign('file_id').references('id').inTable('files')
        }),

    ])

}

exports.down = function(knex) {

}
