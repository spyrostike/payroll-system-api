
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('mail_transaction_sent_results',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary()
            table.uuid('mail_transaction_id')
            table.uuid('user_id')
            table.text('sent_error_message')
            table.enu('category', null, {
                useNative: true,
                enumName: 'FILE_CATEGORY',
                schemaName: 'public',
                existingType: true
            })
            table.enu('send_status', [ 'SUCCESSED', 'FAILED' ], {
                useNative: true,
                enumName: 'SEND_STATUS',
                schemaName: 'public',
            })
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('updated_by')

            table.foreign('mail_transaction_id').references('id').inTable('mail_transactions')
        }),

    ])

}

exports.down = function(knex) {

}
