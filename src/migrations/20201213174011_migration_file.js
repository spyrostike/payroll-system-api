
exports.up = async (knex) => {
    await knex.raw('create extension if not exists "uuid-ossp"')

    await Promise.all([

        knex.schema.createTable('files',  (table) => {
            table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary()
            table.string('path', 20)
            table.string('name', 100)
            table.integer('record')
            table.string('type', 50)
            table.enu('category', [ 'PAY-SLIP', 'USER-IMPORT' ], {
                useNative: true,
                enumName: 'FILE_CATEGORY',
                schemaName: 'public',
            })
            table.integer('size')
            table.enu('status', null, {
                useNative: true,
                enumName: 'STATUS',
                schemaName: 'public',
                existingType: true
            })
            table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('created_by')
            table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'))
            table.uuid('updated_by')
        }),

    ])

}

exports.down = function(knex) {

}
