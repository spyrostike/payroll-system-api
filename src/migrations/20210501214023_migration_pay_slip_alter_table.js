
exports.up = async (knex) => {
  await knex.raw('create extension if not exists "uuid-ossp"')

  await Promise.all([

    knex.schema.alterTable('pay_slips',  (table) => {
      table.string('period', 7)
    })
  ])
}

exports.down = async (knex) => {
  await knex.raw('create extension if not exists "uuid-ossp"')

  await Promise.all([
    knex.schema.alterTable('pay_slips', (table) => {
      table.string('period', 7)
    })
  ])

}
