
exports.up = async (knex) => {
  await knex.raw('create extension if not exists "uuid-ossp"')

  await Promise.all([

    knex.schema.alterTable('pay_slips', (table) => {
      table.float('annual_income_year', 14, 2)
      table.float('taxing_saving_year', 14, 2)
      table.float('fund_saving_year', 14, 2)
      table.float('social_security_year', 14, 2)
      table.float('other_deduction_year', 14, 2)
    })
  ])
}

exports.down = async (knex) => {
  await knex.raw('create extension if not exists "uuid-ossp"')

  await Promise.all([
    knex.schema.alterTable('pay_slips', (table) => {
      table.float('annual_income_year', 14, 2)
      table.float('taxing_saving_year', 14, 2)
      table.float('fund_saving_year', 14, 2)
      table.float('social_security_year', 14, 2)
      table.float('other_deduction_year', 14, 2)
    })
  ])

}
