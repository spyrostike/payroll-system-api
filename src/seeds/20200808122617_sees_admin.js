const bcrypt = require('bcryptjs')

exports.seed = async (knex) => {
    // Deletes ALL existing entries
    return knex('admins').del()
    .then( async () => {
      // Inserts seed entries

      const password = '123456'
      const privateKey = await bcrypt.genSalt(15)
      const encodePassword = await bcrypt.hash(password, privateKey)

      return knex('admins').insert([
        {
          username: 'administrator',
          password: encodePassword,
          // private_key: privateKey,
          first_name: 'เจ้าหน้าที่',
          last_name: 'ดูแลระบบ',
          tel: '0812345678',
          role: 'super-admin',
          status: 'active',
          gender: 'male'
        }
      ])
    })
  }