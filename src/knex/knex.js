const environment = process.env.ENVIRONMENT || 'development'
const config = require('../knexfile.js')[environment]
const { attachPaginate } = require('knex-paginate')
const knex = require('knex')(config)
attachPaginate()
module.exports = knex

// knex migrate:latest
// knex seed:run

// {
//   "username": "{{$randomFirstName}}",
//   "password": "{{$randomPassword}}",
//   "displayName": "{{$randomFullName}}"
// }