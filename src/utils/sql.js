export const getSearchPagerFromRequest = (req) => {
  const perPage =
      req.query['perPage'] && req.query['perPage'] !== ''
          ? req.query['perPage']
          : 10
  const currentPage =
      req.query['currentPage'] && req.query['currentPage'] !== ''
          ? req.query['currentPage']
          : 0
  return {
      perPage,
      currentPage,
      isLengthAware: true,
  }
}