import PDFDocument from 'pdfkit'
import fs from 'fs'
import dayjs from 'dayjs'

export const generateSlipPDF = (paySlips, period) => {
  for (var i = 0; i < paySlips.length; i++) {
    const paySlip = paySlips[i]
    paySlip.pdfPath = `${process.env.PDF_PATH}/Payslip-${dayjs().format('YYYYMMDD')}-${paySlip.employeeCode}.pdf`
    console.log('generateSlipPDF', paySlip.idCardNo + paySlip.employeeCode)

    // Create a document
    const doc = new PDFDocument({ userPassword: paySlip.idCardNo + paySlip.employeeCode })

    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    doc.pipe(fs.createWriteStream(paySlip.pdfPath))

    //title
    doc
    .font('./fonts/ANGSA.ttf')
    .fontSize(15)
    .text('ใบจ่ายเงินเดือน', 250, 5)

    //company
    doc
    .font('./fonts/ANGSA.ttf')
    .fontSize(15)
    .text('บริษัท คินเจน ไบโอเทค จำกัด', 210, 25)

    //employee detail
    doc.fontSize(10).text('รหัส', 10, 50)
    doc.text(paySlip.employeeCode, 25, 50)
    doc.text('ชื่อ-สกุล', 170, 50)
    doc.text(paySlip.employeeName, 196, 50)
    doc.text('แผนก', 320, 50)
    doc.text(paySlip.departmentName, 346, 50)
    doc.text('เดือนที่', 460, 50)
    doc.text(period, 480, 50)

    //header th
    doc.text('รายได้', 70, 80)
    doc.text('จำนวน', 170, 80)
    doc.text('จำนวนเงิน', 250, 80)
    doc.text('รายการหัก', 350, 80)
    doc.text('จำนวนเงิน', 450, 80)
    doc.text('วันที่จ่าย', 550, 80)

    //header en
    doc.text('Earnings', 65, 90)
    doc.text('Number', 170, 90)
    doc.text('Amount', 253, 90)
    doc.text('Deductions', 348, 90)
    doc.text('Amount', 453, 90)
    doc.text('Payroll Date', 545, 90)

    //detail
    //line 1
    doc.text('อัตรา', 30, 110)
    doc.text(formatMoney(paySlip.salary), 0, 110, { align: 'right', width: 295 })
    doc.text('หักขาดงาน', 330, 110)
    doc.text(formatMoney(paySlip.absentDays), 0, 110, { align: 'right', width: 490 })

    //line 2
    doc.text('เงินเดือน', 30, 125)
    doc.text(formatMoney(paySlip.workDay), 0, 125, { align: 'right', width: 200 })
    doc.text(formatMoney(paySlip.salary), 0, 125, { align: 'right', width: 295 })
    doc.text('หักลาป่วย', 330, 125)
    doc.text('0.00', 0, 125, { align: 'right', width: 490 })
    doc.text(dayjs().format('DD/MM/YYYY'), 548, 125)

    //line 3
    doc.text('ค่าล่วงเวลา 1 เท่า', 30, 140)
    // doc.text('0.00', 0, 140, { align: 'right', width: 200 })
    doc.text(formatMoney(paySlip.otx1), 0, 140, { align: 'right', width: 295 })
    doc.text('หักมาสาย', 330, 140)
    doc.text(formatMoney(paySlip.lateDays), 0, 140, { align: 'right', width: 490 })

    //line 4
    doc.text('ค่าล่วงเวลา 1.5 เท่า', 30, 155)
    // doc.text('0.00, 0, 155, { align: 'right', width: 200 })
    doc.text(formatMoney(paySlip.otx1dot5), 0, 155, { align: 'right', width: 295 })
    doc.text('หักลากิจ', 330, 155)
    doc.text('0.00', 0, 155, { align: 'right', width: 490 })

    //line 5
    doc.text('ค่าล่วงเวลา 2 เท่า', 30, 170)
    // doc.text('0.00', 0, 170, { align: 'right', width: 200 })
    doc.text(formatMoney(paySlip.otx2), 0, 170, { align: 'right', width: 295 })
    doc.text('หักลาอื่น', 330, 170)
    doc.text(formatMoney(paySlip.deductOther), 0, 170, { align: 'right', width: 490 })

    //line 6
    doc.text('ค่าล่วงเวลา 3 เท่า', 30, 185)
    // doc.text('0.00', 0, 185, { align: 'right', width: 200 })
    doc.text(formatMoney(paySlip.otx3), 0, 185, { align: 'right', width: 295 })
    doc.text('หักสวัสดิการ', 330, 185)
    doc.text(formatMoney(paySlip.otherWelfare), 0, 185, { align: 'right', width: 490 })

    //line 7
    doc.text('ค่าล่วงเวลาอื่นๆ', 30, 200)
    doc.text(formatMoney(paySlip.otOther), 0, 200, { align: 'right', width: 295 })
    doc.text('หักจ่ายอื่นๆ', 330, 200)
    doc.text(formatMoney(paySlip.deductOther), 0, 200, { align: 'right', width: 490 })

    //line 8
    doc.text('สวัสดิการอื่นๆ', 30, 215)
    doc.text(formatMoney(paySlip.welfare), 0, 215, { align: 'right', width: 295 })
    doc.text('หักประกันแรกเข้า', 330, 215)
    doc.text(formatMoney(paySlip.securityDeposit), 0, 215, { align: 'right', width: 490 })

    //line 9
    doc.text('เงินได้จากหน้าที่', 30, 230)
    doc.text(formatMoney(paySlip.dutyCost), 0, 230, { align: 'right', width: 295 })
    doc.text('หักเงินกู้', 330, 230)
    doc.text(formatMoney(paySlip.deductLoan), 0, 230, { align: 'right', width: 490 })

    //line 10
    doc.text('เงินรางวัลพิเศษ', 30, 245)
    doc.text(formatMoney(paySlip.reward), 0, 245, { align: 'right', width: 295 })
    doc.text('หักกองทุนสำรองเลี้ยงชีพ', 330, 245)
    doc.text(formatMoney(paySlip.deductFund), 0, 245, { align: 'right', width: 490 })

    //line 11
    doc.text('เงินได้อื่นๆ', 30, 260)
    doc.text(formatMoney(paySlip.otherIncome), 0, 260, { align: 'right', width: 295 })
    doc.text('หักสมทบประกันสังคม', 330, 260)
    doc.text(formatMoney(paySlip.deductSocialSociety), 0, 260, { align: 'right', width: 490 })

    //line 12
    doc.text('หักภาษี', 330, 275)
    doc.text(formatMoney(paySlip.deductTax), 0, 275, { align: 'right', width: 490 })

    //net to pay
    doc.text('เงินรับสุทธิ', 548, 245)
    doc.text('Net To Pay', 548, 255)

    //total header th
    doc.text('รวมเงินได้', 100, 295)
    doc.text('รวมรายการหัก', 350, 295)

    //total header en
    doc.text('Total Earnings', 90, 305)
    doc.text('Total Deductions', 346, 305)
    doc.text(formatMoney(paySlip.salaryIncludeOther), 0, 305, { align: 'right', width: 295 })
    doc.text(formatMoney(paySlip.totalDeduct), 0, 305, { align: 'right', width: 490 })
    doc.text(formatMoney(paySlip.salaryNet), 0, 305, { align: 'right', width: 575 })

    //summary header
    doc.fontSize(10).text('เงินได้สะสมต่อปี', 30, 325)
    doc.text('ภาษีสะสมต่อปี', 150, 325)
    doc.text('เงินสะสมกองทุนต่อปี', 250, 325)
    doc.text('เงินประกันสังคมต่อปี', 350, 325)
    doc.text('ค่าลดหย่อนอื่นๆ', 440, 325)

    //summary detail
    doc.text(formatMoney(paySlip.annualIncomeYear), 0, 340, { align: 'center', width: 105 })
    doc.text(formatMoney(paySlip.taxingSavingYear), 0, 340, { align: 'center', width: 338 })
    doc.text(formatMoney(paySlip.fundSavingYear), 0, 340, { align: 'center', width: 555 })
    doc.text(formatMoney(paySlip.socialSecurityYear), 0, 340, { align: 'center', width: 755 })
    doc.text(formatMoney(paySlip.otherDeductionYear), 0, 340, { align: 'center', width: 920 })



    // Finalize PDF file
    doc.end()
  }

}

const formatMoney = (value) => {
  return value.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}