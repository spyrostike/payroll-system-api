import fs from 'fs'
import mkdirp from 'mkdirp'


export const checkAndMkDir = (dir, callback) => {
    fs.access(dir, fs.constants.R_OK, (err) => {
        if (err) {
            mkdirp(dir, (err) => {
                if(err) {
                    console.log('Error in folder creation')
                    callback()
                }
                callback()
            })
        } else {
            callback()
        }
    })
}
