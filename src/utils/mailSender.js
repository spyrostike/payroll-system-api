import nodemailer from 'nodemailer'

// smtp.office365.com
// Port: 587

const transporter = nodemailer.createTransport({
  service: 'outlook',
  port: 587,
  auth: {
    user: 'noreply@kingenbiotech.com', // your email
    pass: 'P@ssw0rd' // your email password
  },
  tls: {
    rejectUnauthorized: false
  }
})

export const sendMail = async (mailOptions) => {
  try {
    await transporter.sendMail(mailOptions)
    return { error: null }
  } catch (e) {
    return { error: e }
  }
}

export const sendMailPaySlip = async (paySlips, period) => {
  const resultArr = []
  let sentSuccess = 0
  let sentFail = 0
  let sentTotal = 0

  for (var i = 0; i < paySlips.length; i++) {
    const paySlip = paySlips[i]

    let mailOptions = {
      from: 'noreply@kingenbiotech.com',                // sender
      to: paySlip.email,                // list of receivers
      subject: 'การจัดส่งใบจ่ายเงินเดือน อิเล็กทรอนิกส์ (E-Slip)',              // Mail subject
      html: `
        เรื่อง การจัดส่งใบจ่ายเงินเดือน อิเล็กทรอนิกส์ (E-Slip)<br/>
        เรียน พนักงาน<br/>
        <p><dd>ส่วนงานทรัพยากรบุคคลและธุรการขอนำส่งใบจ่ายเงินเดือนอิเล็กทรอนิกส์ งวดที่ ${period} ทางบริษัทฯ ได้แนบเอกสารในอีเมลล์
        ฉบับนี้ โดยคลิกไฟล์ (PDF File) และใส่รหัสส่วนตัวของท่านตามวิธีดังต่อไปนี้</p>
        <p><dd>กรุณาใส่รหัสผ่านในรูปแบบ XxxxxxxxxxxxxYyyyy
        xxxxxxxxxxxxx : รหัสบัตรประจำตัวประชาชน 13 หลัก</p>
        <p>yyyyy : รหัสพนักงาน 5 หลัก</p>
        <p>ตัวอย่างเช่น 110150072201010041</p>
        ขอแสดงความนับถือ
      `,
      attachments: [
          {
              path: paySlip.pdfPath
          }
      ]   // HTML body
    }

    const result = await sendMail(mailOptions)
    if (!result?.error) {
      resultArr.push({ id:  paySlip.id, sentStatus: 'SUCCESSED', userId: paySlip.userId,   })
      sentSuccess++
    } else {
      resultArr.push({ id:  paySlip.id, sentStatus: 'FAILED', userId: paySlip.userId, error: result.error })
      sentFail++
    }

    sentTotal++
  }

  return { sentSuccess: sentSuccess, sentFail: sentFail, sentTotal: sentTotal, paySlips: resultArr }
}