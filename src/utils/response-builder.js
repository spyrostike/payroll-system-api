export const makeRespBadRequest = (res, errors) =>
  res.status(400).json({
    status: 'fail',
    result: null,
    messages: errors.array
      ? errors
        .array()
          .map(({ param, msg }) => param + ' :' + msg)
      : {},
})

export const makeRespBadRequestText = (res, message) =>
  res.status(400).json({
    status: 'fail',
    result: null,
    messages: [message],
})

export const makeRespSuccess = (res, result, message) =>
  res.status(200).json({
    status: 'success',
    result: result,
    messages: [message ? message : 'transaction successful'],
})