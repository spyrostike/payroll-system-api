import uuidValidator from 'uuid-validate'

export const isUUID = (uuid) => uuidValidator(uuid)
