import 'dotenv/config'
import cors from 'cors'
import bodyParser, { json } from 'body-parser'
import express from 'express'
import http from 'http'
import fs from 'fs'
import winston, { createLogger } from 'winston'
import expressWinston from 'express-winston'
// import { createLogger, format, transports } from 'winston'
import path from 'path'
import { writeLog } from './middleware/logger'
import { v4 as uuidv4 } from 'uuid'

const app = express()
const server = http.createServer(app)


import initRouter from './routes'

require('winston-daily-rotate-file')
require('express-async-errors')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const LOG_PATH = process.env.LOG_PATH

if (!fs.existsSync(LOG_PATH)) {
  fs.mkdirSync(LOG_PATH)
}

const dailyRotateFileTransport = new winston.transports.DailyRotateFile({
  filename: `${LOG_PATH}/%DATE%-results.log`,
  datePattern: 'YYYY-MM-DD'
})

expressWinston.requestWhitelist.push('body')
expressWinston.responseWhitelist.push('body')


const logger = createLogger({
  // change level if in dev environment versus production
  level: process.env.NODE_ENV === 'development' ? 'verbose' : 'info',
  format:  winston.format.combine(
    winston.format.label({ label: path.basename(process.mainModule.filename) }),
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
      new winston.transports.Console({
        level: 'info',
        format:  winston.format.combine(
          winston.format.colorize(),
          winston.format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
          )
        )
      }),
      dailyRotateFileTransport
  ],
})

app.use((req, res, next) => {
  req.body.transactionId = uuidv4()
  console.log('req.body.transactionId', req.body.transactionId)
  req.logger = logger
  next()
})

app.use(expressWinston.logger({
  level: 'info',
  format: winston.format.combine(
    winston.format.label({ label: path.basename(process.mainModule.filename) }),
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.printf(info => `${info.timestamp} ${info.level}: transacionId: ${info.meta.req.body.transactionId} ${JSON.stringify(info, null, 2)}`)
  ),
  transports: [ dailyRotateFileTransport ]
}))

initRouter(app)

app.use((err, req, res, next) => {
  console.log('error=>', err)

  res.status(500).json({
      result: err,
      messages: [err.stack || err],
      trace: {},
  })
})

server.listen(process.env.PORT, () => {
  console.log(`app listening on port ${process.env.PORT}!`)
})

const shutDown = () => {
  console.log('Received kill signal, shutting down gracefully')
  server.close(() => {
      console.log('Closed out remaining connections')
      process.exit(0)
  })

  setTimeout(() => {
      console.error(
          'Could not close connections in time, forcefully shutting down',
      )
      process.exit(1)
  }, 10000)
}

process.on('SIGTERM', shutDown)
process.on('SIGINT', shutDown)