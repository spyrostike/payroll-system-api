const intenalServiceVeify = () => async (req, res, next) => {
    console.log('req.currentUserId', req.currentUserId)
    if (req.currentUserId) {
        next()
        return
    }

    const token = req.headers['internal-token'] || ''

    console.log('xxxx', process.env.INTERNAL_KEY === token)

    if (process.env.INTERNAL_KEY === token) {
        next()
        return
    }

    res.status(403).json({
        result: null,
        messages: [`internal-token invalid`],
    })
}

export default intenalServiceVeify
