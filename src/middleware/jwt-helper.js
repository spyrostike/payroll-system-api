import jwt from 'jsonwebtoken'
const knex = require('../knex/knex.js')

export const createAccessToken = (user) => {
  const token = jwt.sign(
    {
      exp:  Math.floor(Date.now() / 1000) + 60 * (60 * (24 * 3)),
      userId: user.id,
      role: user.role,
      displayName: user.displayName,
    },
    process.env.JWT_SECRET_KEY,
  )
  return token 
}

export const jwtVerify = (publicOnly = false, checkSuperAdmin = false) => async (req, res, next) => {
    // by pass jwt verify when internal service

    const appId = req.headers['appId'] || req.headers['appid'] || ''

    if (!appId || appId !== process.env.APP_ID) {
        res.status(403).json({
            result: null,
            messages: ['Error somethings wrong.'],
        })
        return
    }

    // if (!publicOnly && req.headers['internal-token']) {
    //     next()
    //     return
    // }

    const accessToken = req.headers['accessToken'] || req.headers['accesstoken'] || ''

    if (!accessToken) {
        res.status(403).json({
            result: null,
            messages: ['this operation require access token'],
        })
        return
    }

    const tokenObj = jwt.decode(accessToken, process.env.JWT_SECRET_KEY)

    if (checkSuperAdmin) {
        let [admin] = await knex('admins').where(
            'id',
            tokenObj.userId
        ).andWhere('status', 'active')

        if (!admin && admin?.role !== 'super-admin') {
            res.status(403).json({
                result: null,
                messages: ['Cannot access this method.'],
            })
            return
        }
    }


    const { isAuthorized, decoded, err } = await new Promise(
        (resolve) => {
            jwt.verify(
                accessToken,
                process.env.JWT_SECRET_KEY,
                {
                    ignoreExpiration: true,
                },
                (err, decoded) => {
                    resolve({
                        isAuthorized: err == null,
                        decoded,
                        err,
                    })
                },
            )
        },
    )

    if (!isAuthorized) {
        res.status(403).json({
            result: null,
            messages: [`${err.name} : ${err.message}`],
        })
        return
    }

    req.currentUserId = decoded.userId

    next()
}
