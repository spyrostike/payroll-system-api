import asyncHandler from 'express-async-handler'
import { validationResult } from 'express-validator'
import bcrypt from 'bcryptjs'
import {
    makeRespBadRequestText,
    makeRespSuccess,
    makeRespBadRequest,
} from '../utils/response-builder'
import { createAccessToken } from '../middleware/jwt-helper'

const knex = require('../knex/knex.js')

export const admin = asyncHandler(async (req, res) => {
    await authentication('admins', req, res)
})

export const user = asyncHandler(async (req, res) => {
    await authentication('users', req, res)
})

const authentication = async (type = 'users', req, res) => {
  // validate request body
  const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return makeRespBadRequest(res, errors)
    }

    const requestBody = req.body

    const [user] = await knex(type).where(
        'username',
        requestBody.username,
    )

    if (!user) {
        return makeRespBadRequestText(
            res,
            `ไม่พบผู้ใช้งาน ${requestBody.username}`,
        )
    }

    if (user.status === 'inactive') {
        return makeRespBadRequestText(
            res,
            `ผู้ใช้ยังไม่ถูกเปิดใช้งาน`,
        )
    }

    const { password } = user
    // $2a$15$bpLLGDnI9IH6elK3wxW5zuQSVwRGE4lgtR9xkDngLmLZkAk137NRi
    // $2a$15$bpLLGDnI9IH6elK3wxW5zu
    const match = await bcrypt.compare(requestBody.password, password)

    if (!match) {
        return makeRespBadRequestText(res, 'รหัสผ่านไม่ถูกต้อง')
    }

    const accessToken = createAccessToken(user)
    delete user.password
    return makeRespSuccess(res, { accessToken: accessToken, userProfile: user })
}