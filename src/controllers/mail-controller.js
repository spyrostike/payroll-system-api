import { validationResult } from 'express-validator'
import asyncHandler from 'express-async-handler'
import dayjs from 'dayjs'
import {
  makeRespBadRequestText,
  makeRespSuccess,
  makeRespBadRequest,
} from '../utils/response-builder'
import { isUUID } from '../utils/validator'
import { generateSlipPDF } from '../utils/pdfHelper'
import { sendMailPaySlip } from '../utils/mailSender'
import { getSearchPagerFromRequest } from '../utils/sql'

const knex = require('../knex/knex.js')

export const sendPaySlip = asyncHandler(async (req, res) => {
  // validate request body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      return makeRespBadRequest(res, errors)
  }

  const { fileId } = req.params
  const requestBody = req.body

  const year = parseInt(dayjs().format('YYYY')) + 543
  const period = requestBody.period ? requestBody.period : `${dayjs().format('MM')}/${year}`

  console.log('requestBody', fileId, requestBody)

  const file = await knex('files')
    .select({
      id: 'id',
      name: 'name',
      record: 'record',
      type: 'type',
      category: 'category',
      status: 'status',
      createdBy: 'created_by',
      createdAt: 'created_at',
      updateBy: 'updated_by',
      updatedAt: 'updated_at'
    })
    .where('status', 'active')
    .andWhere('id', fileId)
    .first()

    if (!file) {
      return makeRespBadRequestText(
          res,
          `ไม่พบไฟล์ในฐานข้อมูล`,
      )
    }

    const users = await knex('users')
      .select({
        id: 'id',
        code: 'code',
        username: 'username',
        firstName: 'first_name',
        idCardNo: 'id_card_no',
        lastName: 'last_name',
        branchId: 'branch_id',
        departmentId: 'department_id',
        positionId: 'position_id',
        age: 'age',
        address: 'address',
        email: 'email',
        gender: 'gender',
        tel: 'tel',
        status: 'status',
        createdBy: 'created_by',
        createdAt: 'created_at',
        updateBy: 'updated_by',
        updatedAt: 'updated_at'
      })
      .where('status', 'active')
      .whereIn('code', requestBody.employees)

    const userCodes = users.map(user =>  user.code)

    const paySlips = await knex({ ps: 'pay_slips' })
      .select({
        id: 'ps.id',
        branchCode: 'ps.branch_code',
        departmentCode: 'ps.department_code',
        departmentName: 'ps.department_name',
        employeeCode: 'ps.employee_code',
        employeeName: 'ps.employee_name',
        includeDepartment: 'ps.include_department',
        includeBranch: 'ps.include_branch',
        workDay: 'ps.work_day',
        salary: 'ps.salary',
        positionCost: 'ps.position_cost',
        otx1: 'ps.otx1',
        otx1dot5: 'ps.otx1_5',
        otx2: 'ps.otx2',
        otx3: 'ps.otx3',
        otOther: 'ps.ot_other',
        shiftCost: 'ps.shift_cost',
        dutyCost: 'ps.duty_cost',
        welfare: 'ps.welfare',
        reward: 'ps.reward',
        bonus: 'ps.bonus',
        otherIncome: 'ps.other_income',
        salaryIncludeOther: 'ps.salary_include_other',
        absentDays: 'ps.absent_days',
        lateDays: 'ps.late_days',
        deductWorkOffEarly: 'ps.deduct_work_off_early',
        deductWorfImpaired: 'ps.deduct_worf_impaired',
        deductAdvance: 'ps.deduct_advance',
        otherWelfare: 'ps.deduct_welfare',
        deductOther: 'ps.deduct_other',
        deductTax: 'ps.deduct_tax',
        deductFund: 'ps.deduct_fund',
        deductSocialSociety: 'ps.deduct_social_society',
        securityDeposit: 'ps.security_deposit',
        deductLoan: 'ps.deduct_loan',
        deductInstallment: 'ps.deduct_installment',
        totalDeduct: 'ps.total_deduct',
        salaryNet: 'ps.salary_net',
        annualIncomeYear: 'ps.annual_income_year',
        taxingSavingYear: 'ps.taxing_saving_year',
        fundSavingYear: 'ps.fund_saving_year',
        socialSecurityYear: 'ps.social_security_year',
        otherDeductionYear: 'ps.other_deduction_year',
        fileId: 'ps.file_id',
        email: 'us.email',
        userId: 'us.id',
        idCardNo: 'us.id_card_no'
      })
      .innerJoin('users as us', function() {
        this.on('us.code', '=', 'ps.employee_code')
      })
      .where('ps.file_id', fileId)
      .whereIn('ps.employee_code', userCodes)

      console.log('paySlips', paySlips)
  generateSlipPDF(paySlips, period)

  const sendResult = await sendMailPaySlip(paySlips, period)
  console.log('sendResult', sendResult)

  await knex.transaction(async (trx) => {
    let errorTransaction = null
    let transaction = null
    const [mailTrans] = await knex('mail_transactions')
    .transacting(trx)
    .returning('*')
    .insert({
      file_id: fileId,
      sent_success: sendResult.sentSuccess,
      sent_fail: sendResult.sentFail,
      sent_total: sendResult.sentSuccess,
      period: period,
      category: 'PAY-SLIP',
      status: 'active'
    })
    .then(mailTransRes => {
      transaction = mailTransRes[0]
      console.log('mailTransRes', transaction)
      const records = sendResult.paySlips.map(paySlip => ({
        mail_transaction_id: mailTransRes[0].id,
        user_id: paySlip.userId,
        sent_error_message: paySlip.error,
        category: mailTransRes[0].category,
        send_status: paySlip.sentStatus,
        status: 'active'
      }))

      return knex('mail_transaction_sent_results')
      .insert(records)
      .transacting(trx)
      .returning('id')
    })
    .then(trx.commit)
    .catch(err => {
      console.log('error', err)
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
      errorTransaction = err
      trx.rollback()
    })
    .finally(() => {
      if (errorTransaction) {
        return makeRespBadRequestText(res, errorTransaction)
      } else {
        return makeRespSuccess(res, transaction)
      }
    })

  })

})

export const sendPaySlipByUser = asyncHandler(async (req, res) => {
  // validate request body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      return makeRespBadRequest(res, errors)
  }

  const { fileId } = req.params
  const requestBody = req.body

  // const year = parseInt(dayjs().format('YYYY')) + 543

  // const period = requestBody.period ? requestBody.period : `${dayjs().format('MM')}/${year}`

  console.log('requestBody', fileId, req.currentUserId)

  const [user] = await knex('users')
  .select({
    id: 'id',
    code: 'code',
    username: 'username',
    firstName: 'first_name',
    idCardNo: 'id_card_no',
    lastName: 'last_name',
    branchId: 'branch_id',
    departmentId: 'department_id',
    positionId: 'position_id',
    age: 'age',
    address: 'address',
    email: 'email',
    gender: 'gender',
    tel: 'tel',
    status: 'status',
    createdBy: 'created_by',
    createdAt: 'created_at',
    updateBy: 'updated_by',
    updatedAt: 'updated_at'
  })
  .where('status', 'active')
  .andWhere('id', req.currentUserId)

  if (!user) {
      return makeRespBadRequestText(
          res,
          `ไม่พบข้อมูล user`,
      )
  }

  const file = await knex('files')
    .select({
      id: 'id',
      name: 'name',
      record: 'record',
      type: 'type',
      category: 'category',
      status: 'status',
      createdBy: 'created_by',
      createdAt: 'created_at',
      updateBy: 'updated_by',
      updatedAt: 'updated_at',
      period: 'period'
    })
    .where('status', 'active')
    .andWhere('id', fileId)
    .first()

    if (!file) {
      return makeRespBadRequestText(
          res,
          `ไม่พบไฟล์ในฐานข้อมูล`,
      )
    }

    const paySlips = await knex({ ps: 'pay_slips' })
      .select({
        id: 'ps.id',
        branchCode: 'ps.branch_code',
        departmentCode: 'ps.department_code',
        departmentName: 'ps.department_name',
        employeeCode: 'ps.employee_code',
        employeeName: 'ps.employee_name',
        includeDepartment: 'ps.include_department',
        includeBranch: 'ps.include_branch',
        workDay: 'ps.work_day',
        salary: 'ps.salary',
        positionCost: 'ps.position_cost',
        otx1: 'ps.otx1',
        otx1dot5: 'ps.otx1_5',
        otx2: 'ps.otx2',
        otx3: 'ps.otx3',
        otOther: 'ps.ot_other',
        shiftCost: 'ps.shift_cost',
        dutyCost: 'ps.duty_cost',
        welfare: 'ps.welfare',
        reward: 'ps.reward',
        bonus: 'ps.bonus',
        otherIncome: 'ps.other_income',
        salaryIncludeOther: 'ps.salary_include_other',
        absentDays: 'ps.absent_days',
        lateDays: 'ps.late_days',
        deductWorkOffEarly: 'ps.deduct_work_off_early',
        deductWorfImpaired: 'ps.deduct_worf_impaired',
        deductAdvance: 'ps.deduct_advance',
        otherWelfare: 'ps.deduct_welfare',
        deductOther: 'ps.deduct_other',
        deductTax: 'ps.deduct_tax',
        deductFund: 'ps.deduct_fund',
        deductSocialSociety: 'ps.deduct_social_society',
        securityDeposit: 'ps.security_deposit',
        deductLoan: 'ps.deduct_loan',
        deductInstallment: 'ps.deduct_installment',
        totalDeduct: 'ps.total_deduct',
        salaryNet: 'ps.salary_net',
        annualIncomeYear: 'ps.annual_income_year',
        taxingSavingYear: 'ps.taxing_saving_year',
        fundSavingYear: 'ps.fund_saving_year',
        socialSecurityYear: 'ps.social_security_year',
        otherDeductionYear: 'ps.other_deduction_year',
        fileId: 'ps.file_id',
        email: 'us.email',
        userId: 'us.id',
        idCardNo: 'us.id_card_no',
        period: 'ps.period'
      })
      .innerJoin('users as us', function() {
        this.on('us.code', '=', 'ps.employee_code')
      })
      .where('ps.file_id', fileId)
      .andWhere('ps.employee_code', user.code)

  const period = requestBody.period ? requestBody.period : file.period

  generateSlipPDF(paySlips, period)

  const sendResult = await sendMailPaySlip(paySlips, period)

  await knex.transaction(async (trx) => {
    let errorTransaction = null
    let transaction = null
    const [mailTrans] = await knex('mail_transactions')
    .transacting(trx)
    .returning('*')
    .insert({
      file_id: fileId,
      sent_success: sendResult.sentSuccess,
      sent_fail: sendResult.sentFail,
      sent_total: sendResult.sentSuccess,
      period: period,
      category: 'PAY-SLIP',
      status: 'active'
    })
    .then(mailTransRes => {
      transaction = mailTransRes[0]
      console.log('mailTransRes', transaction)
      const records = sendResult.paySlips.map(paySlip => ({
        mail_transaction_id: mailTransRes[0].id,
        user_id: paySlip.userId,
        sent_error_message: paySlip.error,
        category: mailTransRes[0].category,
        send_status: paySlip.sentStatus,
        status: 'active'
      }))

      return knex('mail_transaction_sent_results')
      .insert(records)
      .transacting(trx)
      .returning('id')
    })
    .then(trx.commit)
    .catch(err => {
      console.log('error', err)
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
      errorTransaction = err
      trx.rollback()
    })
    .finally(() => {
      if (errorTransaction) {
        return makeRespBadRequestText(res, errorTransaction)
      } else {
        return makeRespSuccess(res, transaction)
      }
    })

  })

})

export const getMailTransaction = asyncHandler(async (req, res) => {
  const {
    orderByKey = 'created_at',
    orderByValue = 'desc',
    textSearch = '',
    status = 'active',
    dateFrom = dayjs().format('YYYY-MM-DD'),
    dateTo = dayjs().format('YYYY-MM-DD'), } = req.query

    if (dayjs(dateFrom, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateFrom || dayjs(dateTo, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateTo) {
      return makeRespBadRequestText(
          res,
          `รูปแบบของ ของวันที่ไม่ถูกต้อง`,
      )
    }

    let query = knex.table({ mt: 'mail_transactions' })

    let whereFirst = 'mt.status = :status '

    if (textSearch && textSearch.trim().length > 0) {
      whereFirst += 'and f.name like :textSearch '
    }

    whereFirst += 'and mt.category = :category '

    whereFirst += 'and cast (mt.created_at as date) >= :dateFrom and cast (mt.created_at as date) <= :dateTo '

    const result = await query .select({
      id: 'mt.id',
      fileId: 'mt.file_id',
      sentSuccess: 'mt.sent_success',
      sentFail: 'mt.sent_fail',
      sentTotal: 'mt.sent_total',
      period: 'mt.period',
      category: 'mt.category',
      createdBy: 'mt.created_by',
      createdAt: 'mt.created_at',
      updateBy: 'mt.updated_by',
      updatedAt: 'mt.updated_at',
      fileName: 'f.name'
    })
    .join('files as f', function() {
      this.on('f.id', '=', 'mt.file_id')
    })
    .whereRaw(
      whereFirst,
      {
        status: status,
        textSearch: `${textSearch}%`,
        category: 'PAY-SLIP',
        dateFrom: dateFrom,
        dateTo: dateTo
      }
    )
    .orderBy(`mt.${orderByKey}`, orderByValue)
    .paginate(getSearchPagerFromRequest(req))

    return makeRespSuccess(res, {
      searchParams: {
          ...req.query,
      },
      ...result,
    })
})

export const getMailTransactionInfo = asyncHandler(async (req, res) => {
  // req.query['perPage'] = 10000000
  const { id } = req.params
  const {  orderByKey = 'created_at', orderByValue = 'asc' } = req.query

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  // let query = knex.table('mail_transaction_sent_results')
  let query = knex.table({ mt: 'mail_transactions' })

  const result = await query .select({
    id: 'mt.id',
    fileId: 'mt.file_id',
    sentSuccess: 'mt.sent_success',
    sentFail: 'mt.sent_fail',
    sentTotal: 'mt.sent_total',
    period: 'mt.period',
    category: 'mt.category',
    createdBy: 'mt.created_by',
    createdAt: 'mt.created_at',
    updateBy: 'mt.updated_by',
    updatedAt: 'mt.updated_at',
    fileName: 'f.name'
  })
  .join('files as f', function() {
    this.on('f.id', '=', 'mt.file_id')
  })
  .where('mt.id', id)
  .andWhere('mt.status', 'active')
  .orderBy('mt.created_at', 'asc')
  .first()

  if (result) {
    let query = knex.table({ ms: 'mail_transaction_sent_results' })

    const mailTransactionSentResults = await query .select({
      id: 'ms.id',
      sendStatus: 'ms.send_status',
      sentErrorMessage: 'ms.sent_error_message',
      category: 'ms.category',
      createdBy: 'ms.created_by',
      createdAt: 'ms.created_at',
      updateBy: 'ms.updated_by',
      updatedAt: 'ms.updated_at',
      employeeId: 'us.id',
      employeeFirstName: 'us.first_name',
      employeeLastName: 'us.last_name',
      employeeCode: 'us.code'
    })
    .innerJoin('users as us', function() {
      this.on('us.id', '=', 'ms.user_id')
    })
    .where('ms.status', 'active')
    .andWhere('ms.mail_transaction_id', result.id)

    result.transSentResults = mailTransactionSentResults

  }

  return makeRespSuccess(res, {
    searchParams: {
        ...req.query,
    },
    ...result,
  })

})