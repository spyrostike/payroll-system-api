import asyncHandler from 'express-async-handler'
import csv from 'csv-parser'
import fs from 'fs'
import {
    makeRespBadRequestText,
    makeRespSuccess,
    makeRespBadRequest,
} from '../utils/response-builder'
import { getSearchPagerFromRequest } from '../utils/sql'
import { isUUID } from '../utils/validator'
import dayjs from 'dayjs'
const bcrypt = require('bcryptjs')

const knex = require('../knex/knex.js')


export const getPaySlip = asyncHandler(async (req, res) => {
    const {
        orderByKey = 'name',
        orderByValue = 'asc',
        textSearch = '',
        status = 'active',
        dateFrom = dayjs().format('YYYY-MM-DD'),
        dateTo = dayjs().format('YYYY-MM-DD'),
    } = req.query

    let query = knex.table('files')

    if (dayjs(dateFrom, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateFrom || dayjs(dateTo, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateTo) {
        return makeRespBadRequestText(
            res,
            `รูปแบบของ ของวันที่ไม่ถูกต้อง`,
        )
    }

    let whereFirst = 'status = :status and category = :category '

    if (textSearch && textSearch.trim().length > 0) {
        whereFirst += 'and (name like :textSearch) '
    }

    whereFirst += 'and cast (created_at as date) >= :dateFrom and cast (created_at as date) <= :dateTo '

    const result = await query
    .select({
        id: 'id',
        name: 'name',
        record: 'record',
        type: 'type',
        category: 'category',
        status: 'status',
        createdBy: 'created_by',
        createdAt: 'created_at',
        updateBy: 'updated_by',
        updatedAt: 'updated_at', 
        period: 'period'
    })
    .whereRaw(
        whereFirst,
        {
            status: status,
            textSearch: `${textSearch}%`,
            // type: 'text/csv',
            category: 'PAY-SLIP',
            dateFrom: dateFrom,
            dateTo: dateTo
        }
    )
    .orderBy(orderByKey, orderByValue)
    .paginate(getSearchPagerFromRequest(req))

    return makeRespSuccess(res, {
        searchParams: {
            ...req.query,
        },
        ...result,
    })
})

export const getPaySlipByUserId = asyncHandler(async (req, res) => {
    req.query['perPage'] = 10000000

    const {
        orderByKey = 'created_at',
        orderByValue = 'desc',
        dateFrom = dayjs().format('YYYY-MM-DD'),
        dateTo = dayjs().format('YYYY-MM-DD'),
    } = req.query

    if (dayjs(dateFrom, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateFrom || dayjs(dateTo, 'YYYY-MM-DD').format('YYYY-MM-DD') !== dateTo) {
        return makeRespBadRequestText(
            res,
            `รูปแบบของ ของวันที่ไม่ถูกต้อง`,
        )
    }

    const [user] = await knex('users').where(
        'id',
        req.currentUserId,
    )

    if (!user) {
        return makeRespBadRequestText(
            res,
            `ไม่พบข้อมูล user`,
        )
    }

    let query = knex.table('pay_slips')

    let whereFirst = 'employee_code = :employeeCode '

    whereFirst += 'and cast (created_at as date) >= :dateFrom and cast (created_at as date) <= :dateTo '

    const result = await query
    .select({
        id: 'id',
        branchCode: 'branch_code',
        includeBranch: 'include_branch',
        department_code: 'department_code',
        department_name: 'department_name',
        employeeCode: 'employee_code',
        employeeName: 'employee_name',
        fileId: 'file_id',
        createDate: 'created_at',
        period: 'period'
    })
    .whereRaw(
        whereFirst,
        {
            employeeCode: user.code,
            // type: 'text/csv',
            // category: 'PAY-SLIP',
            dateFrom: dateFrom,
            dateTo: dateTo
        }
    )
    .orderBy(orderByKey, orderByValue)
    .paginate(getSearchPagerFromRequest(req))

    // return makeRespSuccess(res, {
    //     searchParams: {
    //         ...req.query,
    //     },
    //     ...result,
    // })

    return makeRespSuccess(res, {
        // searchParams: {
        //     ...req.query,
        // },
         ...result
    })
})

export const getPaySlipRecord = asyncHandler(async (req, res) => {
    req.query['perPage'] = 10000000
    const { id } = req.params
    const {  orderByKey = 'created_at', orderByValue = 'desc' } = req.query

    if (!isUUID(id)) {
        return makeRespBadRequestText(
            res,
            `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
        )
    }

    let query = knex.table('pay_slips')

    const result = await query
    .select({
        branchCode: 'id',
        includeBranch: 'include_branch',
        department_code: 'department_code',
        department_name: 'department_name',
        employeeCode: 'employee_code',
        employeeName: 'employee_name',
        period: 'period'
    })
    .where('file_id', id)
    .orderBy(orderByKey, orderByValue)
    .paginate(getSearchPagerFromRequest(req))

    return makeRespSuccess(res, {
        searchParams: {
            ...req.query,
        },
        ...result,
    })
})

export const uploadPaySlip = asyncHandler(async (req, res) => {
    const { file, logger } = req
    const rows = []
    const year = parseInt(dayjs().format('YYYY')) + 543
    const period = req.body.period ? req.body.period : `${dayjs().format('MM')}/${year}`

    logger.log('info', 'uploadPaySlip -> ' + JSON.stringify(file, null, 2))

    fs.createReadStream(file.path)
    .pipe(csv())
    .on('data', (data) => rows.push(data))
    .on('end', async () => {
        const { path, filename, size, mimetype } = file
        const split = path.split('/')

        const newFile = {
            path: split[split.length - 2],
            name: filename,
            record: rows.length,
            size: size,
            type: mimetype,
            category: 'PAY-SLIP',
            status: 'active',
            created_by: req.currentUserId,
            period: period
        }

        let fileSuccess = null
        let errorTransaction = null

        await knex.transaction(async (trx) => {
            await knex('files')
            .transacting(trx)
            .returning('*')
            .insert(newFile)
            .then(file => {
                fileSuccess = file[0]
                const records = []

                rows.map(row => {
                    const rowConvertArr = Object.values(row)
                    records.push({
                        branch_code: rowConvertArr[0],
                        department_code: rowConvertArr[1],
                        department_name: rowConvertArr[2],
                        employee_code: rowConvertArr[3],
                        employee_name: rowConvertArr[4],
                        include_department: rowConvertArr[5],
                        include_branch: rowConvertArr[6],
                        work_day: parseFloat(rowConvertArr[7].replace(/,/g, '')),
                        salary: parseFloat(rowConvertArr[8].replace(/,/g, '')),
                        position_cost: parseFloat(rowConvertArr[9].replace(/,/g, '')),
                        otx1: parseFloat(rowConvertArr[10].replace(/,/g, '')),
                        otx1_5: parseFloat(rowConvertArr[11].replace(/,/g, '')),
                        otx2: parseFloat(rowConvertArr[12].replace(/,/g, '')),
                        otx3: parseFloat(rowConvertArr[13].replace(/,/g, '')),
                        ot_other: parseFloat(rowConvertArr[14].replace(/,/g, '')),
                        shift_cost: parseFloat(rowConvertArr[15].replace(/,/g, '')),
                        duty_cost: parseFloat(rowConvertArr[16].replace(/,/g, '')),
                        welfare: parseFloat(rowConvertArr[17].replace(/,/g, '')),
                        reward: parseFloat(rowConvertArr[18].replace(/,/g, '')),
                        bonus: parseFloat(rowConvertArr[19].replace(/,/g, '')),
                        other_income: parseFloat(rowConvertArr[20].replace(/,/g, '')),
                        salary_include_other: parseFloat(rowConvertArr[21].replace(/,/g, '')),
                        absent_days: parseFloat(rowConvertArr[22].replace(/,/g, '')),
                        late_days: parseFloat(rowConvertArr[23].replace(/,/g, '')),
                        deduct_work_off_early: parseFloat(rowConvertArr[24].replace(/,/g, '')),
                        deduct_worf_impaired: parseFloat(rowConvertArr[25].replace(/,/g, '')),
                        deduct_advance: parseFloat(rowConvertArr[26].replace(/,/g, '')),
                        deduct_welfare: parseFloat(rowConvertArr[27].replace(/,/g, '')),
                        deduct_other: parseFloat(rowConvertArr[28].replace(/,/g, '')),
                        deduct_tax: parseFloat(rowConvertArr[29].replace(/,/g, '')),
                        deduct_fund: parseFloat(rowConvertArr[30].replace(/,/g, '')),
                        deduct_social_society: parseFloat(rowConvertArr[31].replace(/,/g, '')),
                        security_deposit: parseFloat(rowConvertArr[32].replace(/,/g, '')),
                        deduct_loan: parseFloat(rowConvertArr[33].replace(/,/g, '')),
                        deduct_installment: parseFloat(rowConvertArr[34].replace(/,/g, '')),
                        total_deduct: parseFloat(rowConvertArr[35].replace(/,/g, '')),
                        salary_net: parseFloat(rowConvertArr[36].replace(/,/g, '')),
                        annual_income_year: parseFloat(rowConvertArr[37].replace(/,/g, '')),
                        taxing_saving_year: parseFloat(rowConvertArr[38].replace(/,/g, '')),
                        fund_saving_year: parseFloat(rowConvertArr[39].replace(/,/g, '')),
                        social_security_year: parseFloat(rowConvertArr[40].replace(/,/g, '')),
                        other_deduction_year: parseFloat(rowConvertArr[41].replace(/,/g, '')),
                        file_id: file[0].id,
                        created_by: req.currentUserId,
                        period: period
                    })
                })

                return knex('pay_slips')
                .insert(records)
                .transacting(trx)
                .returning('id')
            })
            .then(trx.commit)
            .catch(err => {
                req.logger.log('error', `transactionId: \n error: ${err}`)
                errorTransaction = err
                trx.rollback()
            })
            .finally(() => {
                if (errorTransaction) {
                    return makeRespBadRequestText(res, errorTransaction)
                } else {
                    return makeRespSuccess(res, fileSuccess)
                }
            })
        })
    })
})

export const deletedPaySlip = asyncHandler(async (req, res) => {
    const id = req.params.id

    if (!isUUID(id)) {
      return makeRespBadRequestText(
          res,
          `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
      )
    }

    const [user] = await knex('files')
      .where('status', 'active')
      .andWhere('id', id)
      .andWhere('category', 'PAY-SLIP')

    if (user == null) {
      return makeRespBadRequestText(res, `ไม่พบ ${id}`)
    }

    const updateValue = {
      status: 'inactive',
      updated_at: new Date(),
      updated_by: req.currentUserId
    }

    try {
      await knex('files').where('id', id).update(updateValue)
    } catch (e) {
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
    }

    return makeRespSuccess(res, `ไฟล์ ${id} ถูกลบเรียบร้อย`)
})

export const uploadUser = asyncHandler(async (req, res) => {
    const { file, logger } = req

    const rows = []
    console.log('req.body.transactionId', req.body.transactionId)
    logger.log('info', 'uploadUser -> ' + JSON.stringify(file, null, 2))

    fs.createReadStream(file.path)
    .pipe(csv())
    .on('data', (data) => rows.push(data))
    .on('end', async () => {
        const { path, filename, size, mimetype } = file
        const split = path.split('/')
        const newFile = {
            path: split[split.length - 2], 
            name: filename,
            record: rows.length,
            size: size,
            type: mimetype,
            category: 'USER-IMPORT',
            status: 'active',
            created_by: req.currentUserId
        }

        let fileSuccess = null
        let errorTransaction = null

        await knex.transaction(async (trx) => {
            const password = '123456'
            const privateKey = await bcrypt.genSalt(15)
            const encodePassword = await bcrypt.hash(password, privateKey)

            await knex('files')
            .transacting(trx)
            .returning('*')
            .insert(newFile)
            .then(file => {
                fileSuccess = file[0]
                const records = []

                rows.map((row) => {
                    console.log('row', row)
                    const rowConvertArr = Object.values(row)
                    // console.log('rowConvertArr', rowConvertArr)

                    records.push({
                        code: rowConvertArr[0],
                        username: rowConvertArr[1],
                        password: encodePassword,
                        first_name: rowConvertArr[3],
                        last_name: rowConvertArr[4],
                        id_card_no: rowConvertArr[5].replace(/ +/g, ""),
                        gender: rowConvertArr[6],
                        position_id: rowConvertArr[7],
                        department_id: rowConvertArr[8],
                        branch_id: rowConvertArr[9],
                        address: rowConvertArr[10],
                        email: rowConvertArr[11],
                        tel: rowConvertArr[12],
                        age: 25,
                        role: 'user',
                        status: 'active',
                        created_by: req.currentUserId
                    })

                    // console.log('new user ==>', newUser)
                })

                return knex('users')
                .insert(records)
                .transacting(trx)
                .returning('id')
            })
            .then(trx.commit)
            .catch(err => {
                req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
                errorTransaction = err
                trx.rollback()
            })
            .finally(() => {
                if (errorTransaction) {
                    return makeRespBadRequestText(res, errorTransaction)
                } else {
                    return makeRespSuccess(res, fileSuccess)
                }
            })
        })
    })
})
