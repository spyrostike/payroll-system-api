import { check, validationResult } from 'express-validator'
import asyncHandler from 'express-async-handler'
import bcrypt from 'bcryptjs'
import {
  makeRespBadRequestText,
  makeRespSuccess,
  makeRespBadRequest,
} from '../utils/response-builder'
import { isUUID } from '../utils/validator'

import { getSearchPagerFromRequest } from '../utils/sql'

const knex = require('../knex/knex.js')

export const getList = asyncHandler(async (req, res) => {
  const {
    orderByKey = 'first_name',
    orderByValue = 'asc',
    textSearch = '',
    status = 'active' } = req.query

  let query = knex.table({ us: 'users' })

  let whereFirst = 'us.status = :status '

  if (textSearch && textSearch.trim().length > 0) {
    whereFirst += ' and (us.first_name like :textSearch or us.last_name like :textSearch or us.tel like :textSearch or us.email like :textSearch or us.username like :textSearch) '
  }

  const result = await query
  .select({
    id: 'us.id',
    code: 'us.code',
    username: 'us.username',
    firstName: 'us.first_name',
    idCardNo: 'us.id_card_no',
    lastName: 'us.last_name',
    branchId: 'us.branch_id',
    branchCode: 'b.code',
    branchName: 'b.name',
    departmentId: 'us.department_id',
    departmentCode: 'd.code',
    departmenthName: 'd.name',
    positionId: 'us.position_id',
    positionCode: 'p.code',
    positionName: 'p.name',
    age: 'us.age',
    address: 'us.address',
    email: 'us.email',
    gender: 'us.gender',
    tel: 'us.tel',
    status: 'us.status',
    createdBy: 'us.created_by',
    createdAt: 'us.created_at',
    updateBy: 'us.updated_by',
    updatedAt: 'us.updated_at'
  })
  .leftJoin('branches as b', function() {
    this.on('b.id', '=', 'us.branch_id')
    this.andOnVal('b.status', '=', 'active')
  })
  .leftJoin('departments as d', function() {
    this.on('d.id', '=', 'us.department_id')
    this.andOnVal('d.status', '=', 'active')
  })
  .leftJoin('positions as p', function() {
    this.on('p.id', '=', 'us.position_id')
    this.andOnVal('p.status', '=', 'active')
  })
  .whereRaw(
    whereFirst,
    {
      status: status,
      textSearch: `${textSearch}%`
    }
  )
  .orderBy(orderByKey, orderByValue)
  .paginate(getSearchPagerFromRequest(req))

  return makeRespSuccess(res, {
    searchParams: {
        ...req.query,
    },
    ...result,
  })
})

export const getInfo = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex({ us: 'users' })
    .select({
      id: 'us.id',
      code: 'us.code',
      username: 'us.username',
      firstName: 'us.first_name',
      idCardNo: 'us.id_card_no',
      lastName: 'us.last_name',
      branchId: 'us.branch_id',
      branchCode: 'b.code',
      branchName: 'b.name',
      departmentId: 'us.department_id',
      departmentCode: 'd.code',
      departmenthName: 'd.name',
      positionId: 'us.position_id',
      positionCode: 'p.code',
      positionName: 'p.name',
      age: 'us.age',
      address: 'us.address',
      email: 'us.email',
      gender: 'us.gender',
      tel: 'us.tel',
      status: 'us.status',
      createdBy: 'us.created_by',
      createdAt: 'us.created_at',
      updateBy: 'us.updated_by',
      updatedAt: 'us.updated_at'
    })
    .leftJoin('branches as b', function() {
      this.on('b.id', '=', 'us.branch_id')
      this.andOnVal('b.status', '=', 'active')
    })
    .leftJoin('departments as d', function() {
      this.on('d.id', '=', 'us.department_id')
      this.andOnVal('d.status', '=', 'active')
    })
    .leftJoin('positions as p', function() {
      this.on('p.id', '=', 'us.position_id')
      this.andOnVal('p.status', '=', 'active')
    })
    .where('us.status', 'active')
    .andWhere('us.id', id)

  return makeRespSuccess(res, user)

})

export const add = asyncHandler(async (req, res) => {
  // validate request body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      return makeRespBadRequest(res, errors)
  }

  const requestBody = req.body

  // validate username
  let [user] = await knex('users').where('username', requestBody['username'])
  // .andWhere('status', 'active')

  if (user != null) {
    return makeRespBadRequestText(
        res,
        `ชื่อผู้ใช้งาน ${requestBody['username']} ถูกลงทะเบียนไปแล้ว`,
    )
  }

  [user] = await knex('users').where('code', requestBody['code']).andWhere('status', 'active')

  if (user != null) {
    return makeRespBadRequestText(
        res,
        `รหัสพนักงาน ${requestBody['code']} ถูกลงทะเบียนไปแล้ว`,
    )
  }

  if (requestBody.username.length < 6) {
    return makeRespBadRequestText(
        res,
        `บัญชีชื่อผู้ใช้งาน ไม่ควรน้อยกว่า 6 ตัวอักษร`,
    )
  }

  if (requestBody.password.length < 6) {
    return makeRespBadRequestText(
        res,
        `รหัสผ่าน ไม่ควรน้อยกว่า 6 ตัวอักษร`,
    )
  }

  const newUser = {
    code: requestBody.code,
    username: requestBody.username,
    first_name: requestBody.firstName,
    last_name: requestBody.lastName,
    id_card_no: requestBody.idCardNo,
    branch_id: requestBody.branch,
    department_id: requestBody.department,
    position_id: requestBody.position,
    age: requestBody.age,
    address: requestBody.address,
    email: requestBody.email,
    tel: requestBody.tel,
    role: 'user',
    gender: requestBody.gender,
    status: 'active',
    created_by: req.currentUserId
  }

  const privateKey = await bcrypt.genSalt(15)
  newUser.password = await bcrypt.hash(requestBody.password, privateKey)

  await knex.transaction(async (trx) => {
    try {
      const [id] = await knex('users')
      .transacting(trx)
      .returning('id')
      .insert(newUser)

      await trx.commit()

      return makeRespSuccess(res, { 'userId': id })
    } catch (err) {
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
      await trx.rollback()
      throw err
    }
  })
})

export const update = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('users')
    .where('status', 'active')
    .andWhere('id', id)

  if (user == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const requestBody = req.body

  const updateValue = {
    code: requestBody.code,
    first_name: requestBody.firstName,
    last_name: requestBody.lastName,
    id_card_no: requestBody.idCardNo,
    branch_id: requestBody.branch,
    department_id: requestBody.department,
    position_id: requestBody.position,
    age: requestBody.age,
    address: requestBody.address,
    email: requestBody.email,
    tel: requestBody.tel,
    gender: requestBody.gender,
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  if (requestBody.newPassword) {
    if (requestBody.newPassword.length < 6) {
        return makeRespBadRequestText(
            res,
            `รหัสผ่านไม่ควรน้อยกว่า 6 ตัวอักษร ${id}`,
        )
    }

    const privateKey = await bcrypt.genSalt(15)
    updateValue.password = await bcrypt.hash(requestBody.newPassword, privateKey)
  }

  try {
    await knex('users').where('id', id).update(updateValue)
  } catch (e) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }
  
  delete updateValue.password

  return makeRespSuccess(res, updateValue)
})

export const deleted = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('users')
    .where('status', 'active')
    .andWhere('id', id)

  if (user == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const updateValue = {
    status: 'inactive',
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  try {
    await knex('users').where('id', id).update(updateValue)
  } catch (e) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }

  return makeRespSuccess(res, `ชื่อผู้ใช้งาน ${id} ถูกลบเรียบร้อย`)
})