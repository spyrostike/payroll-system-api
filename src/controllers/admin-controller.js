import { check, validationResult } from 'express-validator'
import asyncHandler from 'express-async-handler'
import bcrypt from 'bcryptjs'
import {
  makeRespBadRequestText,
  makeRespSuccess,
  makeRespBadRequest,
} from '../utils/response-builder'
import { isUUID } from '../utils/validator'

import { getSearchPagerFromRequest } from '../utils/sql'

const knex = require('../knex/knex.js')

export const getList = asyncHandler(async (req, res) => {
  const {
    orderByKey = 'first_name',
    orderByValue = 'asc',
    textSearch = '',
    status = 'active' } = req.query

  let query = knex.table('admins')

  let whereFirst = 'status = :status and role = :role '

  if (textSearch && textSearch.trim().length > 0) {
    whereFirst += 'and (first_name like :textSearch or last_name like :textSearch or tel like :textSearch or email like :textSearch or username like :textSearch) '
  }

  const result = await query
  .select({
    id: 'id',
    username: 'username',
    firstName: 'first_name',
    lastName: 'last_name',
    email: 'email',
    gender: 'gender',
    tel: 'tel',
    status: 'status',
    createdBy: 'created_by',
    createdAt: 'created_at',
    updateBy: 'updated_by',
    updatedAt: 'updated_at'
  })
  .whereRaw(
    whereFirst,
    {
      status: status,
      textSearch: `${textSearch}%`,
      role: 'admin'
    }
  )
  .orderBy(orderByKey, orderByValue)
  .paginate(getSearchPagerFromRequest(req))

  return makeRespSuccess(res, {
    searchParams: {
        ...req.query,
    },
    ...result,
  })
})

export const getInfo = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('admins')
    .select({
      id: 'id',
      username: 'username',
      firstName: 'first_name',
      lastName: 'last_name',
      email: 'email',
      gender: 'gender',
      tel: 'tel',
      status: 'status',
      createdBy: 'created_by',
      createdAt: 'created_at',
      updateBy: 'updated_by',
      updatedAt: 'updated_at'
    })
    .where('status', 'active')
    .andWhere('id', id)

  return makeRespSuccess(res, user)

})

export const add = asyncHandler(async (req, res) => {
  // validate request body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      return makeRespBadRequest(res, errors)
  }

  const requestBody = req.body

  // validate username
  let [user] = await knex('admins').where(
    'username',
    requestBody['username'],
  )

  if (user != null) {
    return makeRespBadRequestText(
        res,
        `ชื่อผู้ใช้งาน ${requestBody['username']} ถูกลงทะเบียนไปแล้ว`,
    )
  }

  if (requestBody.username.length < 6) {
    return makeRespBadRequestText(
        res,
        `บัญชีชื่อผู้ใช้งาน ไม่ควรน้อยกว่า 6 ตัวอักษร`,
    )
  }

  if (requestBody.password.length < 6) {
    return makeRespBadRequestText(
        res,
        `รหัสผ่าน ไม่ควรน้อยกว่า 6 ตัวอักษร`,
    )
  }

  const newUser = {
    username: requestBody.username,
    first_name: requestBody.firstName,
    last_name: requestBody.lastName,
    tel: requestBody.tel,
    role: 'admin',
    gender: requestBody.gender,
    status: 'active',
    created_by: req.currentUserId
  }

  const privateKey = await bcrypt.genSalt(15)
  newUser.password = await bcrypt.hash(requestBody.password, privateKey)

  await knex.transaction(async (trx) => {
    try {
      const [id] = await knex('admins')
      .transacting(trx)
      .returning('id')
      .insert(newUser)

      await trx.commit()

      return makeRespSuccess(res, { 'userId': id })
    } catch (err) {
      await trx.rollback()
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
    }
  })
})

export const update = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('admins')
    .where('status', 'active')
    .andWhere('id', id)

  if (user == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const requestBody = req.body

  const updateValue = {
    first_name: requestBody.firstName,
    last_name: requestBody.lastName,
    tel: requestBody.tel,
    email: requestBody.email,
    gender: requestBody.gender,
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  if (requestBody.newPassword) {
    if (requestBody.newPassword.length < 6) {
        return makeRespBadRequestText(
            res,
            `รหัสผ่านไม่ควรน้อยกว่า 6 ตัวอักษร ${id}`,
        )
    }

    const privateKey = await bcrypt.genSalt(15)
    updateValue.password = await bcrypt.hash(requestBody.newPassword, privateKey)
  }

  try { 
    await knex('admins').where('id', id).update(updateValue)
  } catch (err) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }

  delete updateValue.password

  return makeRespSuccess(res, updateValue)
})

export const deleted = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('admins')
    .where('status', 'active')
    .andWhere('id', id)

  if (user == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const updateValue = {
    status: 'inactive',
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  try {
    await knex('admins').where('id', id).update(updateValue)
  } catch (err) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }

  return makeRespSuccess(res, `ชื่อผู้ใช้งาน ${id} ถูกลบเรียบร้อย`)
})