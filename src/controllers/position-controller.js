import { check, validationResult } from 'express-validator'
import asyncHandler from 'express-async-handler'
import bcrypt from 'bcryptjs'
import {
  makeRespBadRequestText,
  makeRespSuccess,
  makeRespBadRequest,
} from '../utils/response-builder'
import { isUUID } from '../utils/validator'

import { getSearchPagerFromRequest } from '../utils/sql'

const knex = require('../knex/knex.js')

export const getList = asyncHandler(async (req, res) => {
  const {
    orderByKey = 'name',
    orderByValue = 'asc',
    textSearch = '',
    status = 'active' } = req.query

  let query = knex.table('positions')

  let whereFirst = 'status = :status '

  if (textSearch && textSearch.trim().length > 0) {
    whereFirst += ' and (code like :textSearch or name like :textSearch) '
  }

  const result = await query
  .select({
    id: 'id',
    code: 'code',
    name: 'name',
    status: 'status',
    createdBy: 'created_by',
    createdAt: 'created_at',
    updateBy: 'updated_by',
    updatedAt: 'updated_at'
  })
  .whereRaw(
    whereFirst,
    {
      status: status,
      textSearch: `${textSearch}%`
    }
  )
  .orderBy(orderByKey, orderByValue)
  .paginate(getSearchPagerFromRequest(req))

  return makeRespSuccess(res, {
    searchParams: {
        ...req.query,
    },
    ...result,
  })
})

export const getInfo = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [user] = await knex('positions')
    .select({
      id: 'id',
      code: 'code',
      name: 'name',
      status: 'status',
      createdBy: 'created_by',
      createdAt: 'created_at',
      updateBy: 'updated_by',
      updatedAt: 'updated_at'
    })
    .where('status', 'active')
    .andWhere('id', id)

  return makeRespSuccess(res, user)
})

export const add = asyncHandler(async (req, res) => {
  // validate request body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      return makeRespBadRequest(res, errors)
  }

  const requestBody = req.body

  // validate position's name
  let [position] = await knex('positions').where(
    'name',
    requestBody['name'],
  ).andWhere('status', 'active')

  if (position != null) {
    return makeRespBadRequestText(
        res,
        `ชื่อตำแหน่ง ${requestBody['name']} ถูกใช้ซ้ำแล้ว`,
    )
  }

  const newPosition = {
    code: requestBody.code,
    name: requestBody.name,
    status: 'active',
    created_by: req.currentUserId
  }

  await knex.transaction(async (trx) => {
    try {
      const [id] = await knex('positions')
      .transacting(trx)
      .returning('id')
      .insert(newPosition)

      await trx.commit()

      return makeRespSuccess(res, { 'positionId': id })
    } catch (err) {
      req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
      await trx.rollback()
      throw err
    }
  })
})

export const update = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [position] = await knex('positions')
    .where('status', 'active')
    .andWhere('id', id)

  if (position == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const requestBody = req.body

  const updateValue = {
    name: requestBody.name,
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  try {
    await knex('positions').where('id', id).update(updateValue)
  } catch (e) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }

  return makeRespSuccess(res, updateValue)
})

export const deleted = asyncHandler(async (req, res) => {
  const id = req.params.id

  if (!isUUID(id)) {
    return makeRespBadRequestText(
        res,
        `รูปแบบของ ID ไม่ถูกต้อง ${id} `,
    )
  }

  const [position] = await knex('positions')
    .where('status', 'active')
    .andWhere('id', id)

  if (position == null) {
    return makeRespBadRequestText(res, `ไม่พบ ${id}`)
  }

  const updateValue = {
    status: 'inactive',
    updated_at: new Date(),
    updated_by: req.currentUserId
  }

  try {
    await knex('positions').where('id', id).update(updateValue)
  } catch (e) {
    req.logger.log('error', `transactionId: ${req.body.transactionId} \n error: ${err}`)
  }

  return makeRespSuccess(res, `ข้อมูล ${id} ถูกลบเรียบร้อย`)
})