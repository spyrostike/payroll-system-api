FROM node:alpine

WORKDIR /app
COPY ./ /app

RUN rm -rf /usr/local/bin/yarn
RUN npm install -g yarn --force
RUN yarn
RUN yarn build
RUN npm install knex -g

COPY ./package.json /app/dist
COPY ./src/assets /app/dist

WORKDIR /app/dist
RUN yarn

RUN ls

#COPY ./migrations /app/migrations

# RUN yarn migration --env staging

# start app
CMD ["node", "server.js"]

EXPOSE 3005